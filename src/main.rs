use anyhow;
use reqwest::{header::AUTHORIZATION, Client};
use serde::Deserialize;
use std::collections::HashSet;
use std::fs;
use tokio::join;
use url::Url;

#[derive(Deserialize, Debug)]
struct Error {
    error: String,
    message: String,
}

#[derive(Deserialize, Debug)]
struct Response<T> {
    api_version: String,
    method: String,
    fetched: String,
    error: Option<Error>,
    data: Option<T>,
}

#[derive(Deserialize, Debug)]
struct Single<T> {
    object: T,
}

#[derive(Deserialize, Debug)]
struct List<T> {
    current_object_count: i32,
    objects_per_page: i32,
    page_index: i32,
    has_more: bool,
    total_objects: i32,
    total_pages: i32,
    objects: Vec<T>,
}

#[derive(Deserialize, Debug)]
struct Judge {
    name: String,
    start_time: String,
    ping: f64,
    load: f64,
    languages: Vec<String>,
}

#[derive(Deserialize, Debug)]
struct Language {
    id: i32,
    key: String,
    short_name: String,
    common_name: String,
    ace_mode_name: String,
    pygments_name: String,
    code_template: String,
}

#[derive(Deserialize, Debug)]
struct Organization {
    id: i32,
    slug: String,
    short_name: String,
    is_open: bool,
    member_count: i32,
}

#[derive(Deserialize, Debug)]
struct Submission {
    id: i64,
    problem: String,
    user: String,
    date: String,
    language: String,
    time: Option<f64>,
    memory: Option<f64>,
    points: f64,
    result: String,
}

#[derive(Deserialize, Debug)]
struct Problem {
    code: String,
    name: String,
    authors: Vec<String>,
    types: Vec<String>,
    group: String,
    time_limit: f64,
    memory_limit: f64,
    language_resource_limits: Vec<LanguageResourceLimit>,
    points: f64,
    partial: bool,
    short_circuit: bool,
    languages: Vec<String>,
    is_organization_private: bool,
    organizations: Vec<String>,
    is_public: bool,
}

#[derive(Deserialize, Debug)]
struct LanguageResourceLimit {
    language: String,
    time_limit: f64,
    memory_limit: f64,
}

async fn get_raw(
    client: &Client,
    api_key: &str,
    submission_id: &str,
) -> Result<String, reqwest::Error> {
    client
        .get(format!("https://dmoj.ca/src/{}/raw", submission_id))
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .text()
        .await
}

async fn get_problem(
    client: &Client,
    base: &Url,
    api_key: &str,
    problem_code: &str,
) -> Result<Response<Single<Problem>>, reqwest::Error> {
    let url = base.join(&format!("problem/{}", problem_code)).unwrap();
    client
        .get(url)
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .json::<Response<Single<Problem>>>()
        .await
}

async fn get_submissions(
    client: &Client,
    base: &Url,
    api_key: &str,
    page: Option<i32>,
    user: Option<&str>,
    problem: Option<&str>,
    language: Option<&str>,
    result: Option<&str>,
) -> Result<Response<List<Submission>>, reqwest::Error> {
    let mut url = base.join("submissions").unwrap();
    if let Some(p) = page {
        url.query_pairs_mut().append_pair("page", &p.to_string());
    }
    if let Some(x) = user {
        url.query_pairs_mut().append_pair("user", x);
    }
    if let Some(x) = problem {
        url.query_pairs_mut().append_pair("problem", x);
    }
    if let Some(x) = language {
        url.query_pairs_mut().append_pair("language", x);
    }
    if let Some(x) = result {
        url.query_pairs_mut().append_pair("result", x);
    }
    client
        .get(url)
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .json::<Response<List<Submission>>>()
        .await
}

async fn get_organizations(
    client: &Client,
    base: &Url,
    api_key: &str,
    page: Option<i32>,
    is_open: Option<bool>,
) -> Result<Response<List<Organization>>, reqwest::Error> {
    let mut url = base.join("organizations").unwrap();
    if let Some(p) = page {
        url.query_pairs_mut().append_pair("page", &p.to_string());
    }
    if let Some(b) = is_open {
        url.query_pairs_mut()
            .append_pair("is_open", if b { "True" } else { "False" });
    }
    client
        .get(url)
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .json::<Response<List<Organization>>>()
        .await
}

async fn get_languages(
    client: &Client,
    base: &Url,
    api_key: &str,
    page: Option<i32>,
    common_name: Option<&str>,
) -> Result<Response<List<Language>>, reqwest::Error> {
    let mut url = base.join("languages").unwrap();
    if let Some(p) = page {
        url.query_pairs_mut().append_pair("page", &p.to_string());
    }
    if let Some(n) = common_name {
        url.query_pairs_mut().append_pair("common_name", n);
    }
    client
        .get(url)
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .json::<Response<List<Language>>>()
        .await
}

async fn get_judges(
    client: &Client,
    base: &Url,
    api_key: &str,
    page: Option<i32>,
) -> Result<Response<List<Judge>>, reqwest::Error> {
    let mut url = base.join("judges").unwrap();
    if let Some(p) = page {
        url.query_pairs_mut().append_pair("page", &p.to_string());
    }
    client
        .get(url)
        .header(AUTHORIZATION, format!("Bearer {}", api_key))
        .send()
        .await?
        .json::<Response<List<Judge>>>()
        .await
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let base = Url::parse("https://dmoj.ca/api/v2/").unwrap();
    let client = reqwest::Client::new();
    let api_key = "";

    let res = get_submissions(
        &client,
        &base,
        api_key,
        None,
        Some("AlanLN"),
        None,
        None,
        Some("AC"),
    )
    .await?;
    if let Some(data) = res.data {
        let mut set = HashSet::<&str>::new();
        let mut submissions = data.objects;
        submissions.reverse();
        for submission in submissions.iter() {
            let problem_code = submission.problem.as_str();
            if set.contains(problem_code) {
                continue;
            }
            if problem_code.starts_with("ccc98") {
                let id = submission.id.to_string();

                let joined = join!(
                    get_problem(&client, &base, api_key, problem_code),
                    get_raw(&client, api_key, &id)
                );
                let problem = joined.0?;
                let raw = joined.1?;

                if let Some(data) = problem.data {
                    fs::create_dir_all(&format!("{}/{}", "CCC", &problem_code[3..5],))?;
                    set.insert(problem_code);
                    fs::write(
                        &format!(
                            "{}/{}/{}",
                            "CCC",
                            &problem_code[3..5],
                            &format!("{}_{}.cpp", &problem_code[5..], data.object.name)
                        ),
                        raw,
                    )?;
                }
            }
        }
    }
    Ok(())
}
